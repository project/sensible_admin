var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    prefix      = require('gulp-autoprefixer'),
    spritesmith = require('gulp.spritesmith');

 /**
 * @task sass
 * Compile files from scss
 */
gulp.task('sass', function () {
  return gulp.src('scss/style.scss') // the source .scss file
  .pipe(sass()) // pass the file through gulp-sass
  .pipe(prefix(['last 4 versions'], { cascade: true })) // pass the file through autoprefixer
  .pipe(gulp.dest('css')) // output .css file to css folder
});

/**
 * @task watch
 * Watch scss files for changes & recompile
 */
gulp.task('watch', function () {
  gulp.watch(['scss/*.scss', 'scss/**/*.scss'], ['sass']);
});

/**
 * @task gen-sprite
 * Generate sprites from image folder
 */
  gulp.task('gen-sprite', function () {
    var sprite =
      gulp.src('./images/*.png') // png images only
        .pipe(spritesmith({
          imgPath: '../images/generated/sprite.png',
          imgName: 'sprite.png',
          cssName: '_sprite.scss'
        }));
    sprite.img.pipe(gulp.dest('./images/generated/')); // generated path
    sprite.css.pipe(gulp.dest('./scss/')); // generated scss
  });

/**
 * Default task, running just `gulp` will 
 * compile Sass files, watch files.
 */
gulp.task('default', ['watch']);